#!/usr/bin/env python2

# This is to be an interface between the webpage (getting) the parameters, and
# the GetOldTweet library. Kind of an option parser actualy.

import sys
sys.path.append("../libs/")
import GOTP.got as got

import re


def arg_checker (argv) :
    NB_EXPECTED_ARGS = 8
    if len (argv) != NB_EXPECTED_ARGS :
        raise ValueError ("Wrong number of parameters, got {}"
                          .format (len (argv)))
    for i in argv :
        if type (i) != str :
            raise ValueError ("{} is not a string, but a {}"
                              .format (i, type (i)))
    query, from_date, to_date, max_nb_tweets, sort_type, tt, user = argv[1:]
    if not ((len (from_date) == len (to_date) == 10)
            and (from_date[0:4] + to_date[0:4]).isdigit ()
            and (from_date[5:7] + to_date[5:7]).isdigit ()
            and (from_date[8:10] + to_date[8:10]).isdigit ()
            and (from_date[4] == from_date[7] == "-")
            and (to_date[4] == to_date [7] == "-")) :
        raise ValueError ("Date should be in 'YYYY-mm-dd' format")
    if not max_nb_tweets.isdigit () :
        raise ValueError ("Max number of tweets should be a positive in")
    if sort_type not in ["old", "young", "RT", "fav"] :
        raise ValueError ("Sort type not accepted")
    if tt not in ['true', 'True', 'TRUE', 'false', 'False', 'FALSE'] :
        raise ValueError ("6th parameter should be true or false")
    if not re.match ("^[a-zA-Z_@]*$", user) :
        raise ValueError ("username shoudl only contain a-zA-Z0-9_")
    # todo : gerer la non limite de max tweets (ALL -> 0)
    #        ... quoi ? De quoi je parlais a l'epoque ?


def tweet_sorter (tweets, mode) :
    """ Trie une liste de tweets en place selon le mode demande """
    if mode == "fav" :
        champ = lambda t: t.favorites
        decroissant = True
    elif mode == "RT" :
        champ = lambda t: t.retweets
        decroissant = True
    elif mode == "young" :
        champ = lambda t: t.date
        decroissant = True
    elif mode == "old" :
        champ = lambda t: t.date
        decroissant = False
    else :
        raise NotImplemented
    tweets.sort (None, champ, decroissant)


def main (argv) :
    # logger = open ("../logs/getter.logs", "a")
    # logger.write (str (argv) + "\n")
    arg_checker (argv)
    query, from_date, to_date, max_nb_tweets, sort_type, tt, user = argv[1:]
    max_nb_tweets = int (max_nb_tweets)
    tt = tt in ["true", "True", "TRUE"]

    tweetCriteria = got.manager.TweetCriteria().setQuerySearch(query)\
                                               .setSince(from_date)\
                                               .setUntil(to_date)\
                                               .setMaxTweets(max_nb_tweets)\
                                               .setTopTweets(tt)
    if user != "" :
        tweetCriteria.setUsername (user)
        
    tweets = got.manager.TweetManager.getTweets(tweetCriteria)
    tweet_sorter (tweets, sort_type)
    
    output = "We recieved {} tweets".format (len (tweets))
    for i in tweets :
        output += "_"  # This is a tweet separator
        
        link = i.permalink
        link = link.replace ("\\", "\\\\")  # \\ to escape \ escaper
        link = link.replace ("_",  "\_")    # \_ to escape _ separator
        link = link.replace ("|",  "\|")    # \| to escape | separator
        output += link + "|"  # '|' is a field separator
        
        output += str (i.retweets) + "|"
        output += str (i.favorites) + "|"

        short_text = i.text.encode ("ascii", "ignore")
        short_text = short_text.replace ("_", "\_")
        short_text = short_text.replace ("|", "\|")
        output += short_text
    print output


if __name__ == "__main__" :
    main (sys.argv)
