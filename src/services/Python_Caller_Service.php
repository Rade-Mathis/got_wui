<?php

class Python_Caller_Service
{
    /* Return an array, or null */
    public function tweet_getter (string $recherche, string $from_date,
                                  string $to_date, string $max_nb_tweets,
                                  string $sort_type, bool $top_tweets,
                                  string $username)
    {
        $argv = ['../services/getter.py'] ;
        
        /* Check args */
        if ($recherche !== null and $recherche !== ''
            and $this->_is_date ($from_date) and $this->_is_date ($to_date)
            and preg_match ('/\d+/', $max_nb_tweets))
        {
            $argv[1] = $recherche ;
            $argv[2] = $from_date ;
            $argv[3] = $to_date ;
            $argv[4] = $max_nb_tweets ;
            $argv[5] = $sort_type ;
            $argv[6] = $top_tweets ? 'True' : 'False' ;
            $argv[7] = $username ;
        }
        else 
        {
            return null ;
        }
        
        /* enquote all args for safety I suppose */
        for ($i = 1 ; $i < count ($argv) ; ++$i)
        {
            $argv[$i] = $this->_enquote ($argv[$i]) ;
        }
        
        /* Call the script */
        $data_string = $this->_script_caller ($argv);
        $data_array = [''];

        /* First line is 'We recieved xxx tweets' */
        for ($i = 0; $i < strlen ($data_string); ++$i)
        {
            $c = $data_string[$i] ;
            if ($c === '_')
            {
                break ;
            }
            else
            {
                $data_array[0] .= $c ;
            }
        }
        /* Other lines */
        $nb_row = 0 ;
        $nb_col = 0 ;
        for ( ; $i < strlen ($data_string) ; ++$i)
        {
            $char = $data_string[$i] ;
            if ($char === '_')
            { // New row
                array_push ($data_array, ['']) ;
                ++$nb_row ;
                $nb_col = 0 ;
            }
            else if ($char === '|')
            { // New column
                array_push ($data_array[$nb_row], '') ;
                ++$nb_col ;
            }
            else
            { // A writtable char
                if ($char === '\\')
                { // Actualy write next char, since it is escaped
                    $char = $data_string[++$i] ;
                }
                $data_array[$nb_row][$nb_col] .= $char ;
            }
        }
        
        return $data_array ;
    }

    function __construct (\Slim\Container $container)
    {
        $this->logger = $container['logger'] ;
    }

    private function _script_caller (array $argv)
    {
        $command = implode (" ", $argv) ;
        $command = escapeshellcmd ($command) ;
        $this->logger->info ("[command] $command") ;
        $output = shell_exec ($command) ;
        $this->logger->info ("[output] $output") ;
        return $output ;
    }

    private function _enquote (string $query)
    {
        if (strpos ($query, '"') !== false)
        {
            return null ;
        }
        return '"' . $query . '"' ;
    }

    private function _is_date (string $date)
    {
        /* Not good enough. But I'm too lazy to do more. */
        return preg_match ('/\d\d\d\d-\d\d-\d\d/', $date) ;
    }

}
