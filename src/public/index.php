<?php
use \Psr\Http\Message\ServerRequestInterface as Request ;
use \Psr\Http\Message\ResponseInterface as Response ;

if (PHP_SAPI == 'cli-server') {
    // If something actually is a real file, deliver it rather than calling the
    // Slim's router. Actually just return false, and hope for the webserver to
    // do needed shit (if I understand well). Maybe I understand bad.
    $url  = parse_url ($_SERVER['REQUEST_URI']) ;
    $file = __DIR__ . $url['path'] ;
    if (is_file ($file)) {
        return false ;
    }
}

require __DIR__ . '/../vendor/autoload.php' ;

session_start () ;  // WHAT THE HELL IS THIS ?!

$settings = require __DIR__ . '/../settings.php' ;
$app = new \Slim\App ($settings) ;

require __DIR__ . '/../dependencies.php' ;
require __DIR__ . '/../routes.php' ;

$app->run () ;
