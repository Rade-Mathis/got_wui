<?php
use \Psr\Http\Message\ServerRequestInterface as Request ;
use \Psr\Http\Message\ResponseInterface as Response ;

include __DIR__ . '/Controllers/Main_Controller.php' ;

$app->get  ("/got_wui/", Main_Controller::class . ':get_main') ;
$app->post ("/got_wui/", Main_Controller::class . ':post_main') ;

$app->get ('/got_wui/aide', Main_Controller::class . ':get_aide') ;
$app->get ('/got_wui/apropos', Main_Controller::class . ':get_apropos') ;
$app->get ('/got_wui/contact', Main_Controller::class . ':get_contact') ;
