<?php

use \Psr\Http\Message\ServerRequestInterface as Request ;
use \Psr\Http\Message\ResponseInterface as Response ;

include __DIR__ . '/../services/Python_Caller_Service.php' ;

class Main_Controller
{
    function __construct (\Slim\Container $container)
    {
        $this->logger = $container['logger'] ;
        $this->view   = $container['view'] ;
        $this->python_caller_service = new Python_Caller_Service ($container) ;
    }

    /** Display the Main page, when queried through a GET (ie: no form has been
     *  submitted yet. */
    public function get_main (Request $request, Response $response, array $args)
    {
        return $this->render ($response, 'main.twig', [
            "query"         => "A Song of Ice and Fire",
            "from_date"     => "1996-08-01",
            "to_date"       => "2011-04-17",
            "max_nb_tweets" => 7,
            "payload"       => "nothing",
            'top_tweets'    => true,
            'username'      => ''
        ]) ;
    }

    public function post_main (Request $request, Response $response,
                               array $args)
    {
        $post_args = $request->getParsedBody() ;
        $top_tweets = isset ($post_args['top_tweets']) ;

        if ($post_args['query'] === '')
        {
            return $this->render ($response, 'main.twig', [
                'query'         => $post_args['query'],
                'from_date'     => $post_args['from_date'],
                'to_date'       => $post_args['to_date'],
                'max_nb_tweets' => $post_args['max_nb_tweets'],
                'top_tweets'    => $top_tweets,
                'sort_type'     => $post_args['sort_type'],
                'username'      => $post_args['username'],
                'payload'       => 'error',
                'error_message' => 'Le champ "Recherche" ne doit pas être vide.'
            ]);
        }
        
        $data_array = $this->python_caller_service->tweet_getter (
            $post_args['query'], $post_args['from_date'], $post_args['to_date'],
            $post_args['max_nb_tweets'], $post_args['sort_type'], $top_tweets,
            $post_args['username']) ;

        $nb_tweets_recieved_info = $data_array[0] ;
        $tweets_array = array_slice ($data_array, 1, count ($data_array)) ;
        return $this->render ($response, 'main.twig', [
            'query'         => $post_args['query'],
            'from_date'     => $post_args['from_date'],
            'to_date'       => $post_args['to_date'],
            'max_nb_tweets' => $post_args['max_nb_tweets'],
            'top_tweets'    => $top_tweets,
            'sort_type'     => $post_args['sort_type'],
            'username'      => $post_args['username'],
            'payload'       => 'tweets',
            'intro'         => $nb_tweets_recieved_info,
            'tweets'        => $tweets_array
        ]) ;
    }

    public function get_aide (Request $request, Response $response, array $args)
    {
        return $this->render ($response, 'aide.twig', $args) ;
    }

    public function get_apropos (Request $request, Response $response,
                                 array $args)
    {
        return $this->render ($response, 'apropos.twig', $args) ;
    }

    public function get_contact (Request $request, Response $response,
                                 array $args)
    {
        return $this->render ($response, 'contact.twig', $args) ;
    }

    private function render (Response $response, string $template, array $args)
    {
        return $this->view->render ($response, $template, $args) ;
    }
}

