<?php
return [ 'settings' => [
    'displayErrorDetails' => true,
    'logger' => [
        'name'  => 'slim-app',
        'path'  => __DIR__ . '/logs/app.log',
        'level' => \Monolog\Logger::DEBUG,
    ],
    'twig' => [
        'template_path' => __DIR__ . '/templates',
        'cache_path'    => __DIR__ . '/cache'
    ],
] ];
