<?php
use Slim\Container ;

$container = $app->getContainer () ;

$container['logger'] = function (Container $c) {
    $settings = $c->get ('settings') ['logger'] ;
    $logger = new Monolog\Logger ($settings['name']) ;
    $logger->pushProcessor (new Monolog\Processor\UidProcessor ()) ;
    $logger->pushHandler (new Monolog\Handler\StreamHandler (
        $settings['path'], $settings['level'])) ;
    return $logger ;
} ;

$container['view'] = function (Container $c) {
    $settings = $c->get ('settings') ['twig'] ;
    $view = new \Slim\Views\Twig (
        $settings['template_path'],
        ['cache' => $settings['cache_path'],
         'debug' => true]) ;
    $view->addExtension (new Twig_Extension_Debug ()) ;
    
    // Instantiate and add Slim specific extension
    $basePath = rtrim (str_ireplace (
        'index.php', '', $c['request']->getUri ()->getBasePath ()), '/');
    $view->addExtension (new Slim\Views\TwigExtension ($c['router'],
                                                       $basePath)) ;
    $view->getEnvironment()->addGlobal ('session', $_SESSION) ;
    return $view ;
} ;
