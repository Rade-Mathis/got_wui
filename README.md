# got_wui

A web interface (and more) to find old tweets.

## License

This projet is mainly based on
https://github.com/Jefferson-Henrique/GetOldTweets-python which is under an
MIT license by Jefferson Henrique.

Other files are an interface created by RadeMathis and under GPLv3.

These two licenses can be found in the files MITL.txt and gpl-3.0.txt

## About

I don't ensure any security, functionnality or anything: using this software
is on your own and do not involve any of my responsabilities.
